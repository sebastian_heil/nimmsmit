﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Mono.Data.Sqlite;
using System.Data;


namespace OSMDroid1
{
    //erstellt von Stefan Stöhr
    class MetadataDAO
    {
        private String input_file;
        private IDbConnection connection;

        public const String NAME_TYPE_MAX_ZOOM = "max_Zoom";
        public const String NAME_TYPE_MIN_ZOOM = "min_Zoom";
        public const String NAME_TYPE_BOUNDS = "bounds";

        public MetadataDAO(String path) //erstellt von Stefan Stöhr
        {

            this.input_file = path;

            connection = new SqliteConnection("Data Source=" + input_file);
            try
            {
                connection.Open();
            }
            catch (SqliteException e)
            {
                Console.WriteLine(e);
                //Falls Loging verwendet wird, Funktion hier eintragen
            }
        }

        public Metadata getMetadata(String name) //erstellt von Stefan Stöhr
        {
            Metadata meta = null;

            IDbCommand dbcmd = connection.CreateCommand();
            dbcmd.CommandText = "Select value from metadata where name like '%" + name + "%'";

            try
            {
                IDataReader reader = dbcmd.ExecuteReader();

                if (reader.Read())
                {
                    meta = new Metadata(reader.GetString(0), reader.GetString(1));
                }
            }
            catch (SqliteException e)
            {
                Console.WriteLine(e);
                //Falls Loging verwendet wird, Funktion hier eintragen
            }

            return meta;
        }
        public int getMaxZoomLevel() //erstellt von Stefan Stöhr
        {
            int max_Zoom = 0;
            String s = null;
            IDbCommand dbcmd = connection.CreateCommand();
            dbcmd.CommandText = "Select value from metadata where name like '%max_Zoom%'";

            if (connection.State == ConnectionState.Open)
            {
                try
                {
                    IDataReader reader = dbcmd.ExecuteReader();

                    if (reader.Read())
                    {

                        s = reader.GetString(0);
                        max_Zoom = int.Parse(s);

                    }
                }
                catch (SqliteException e)
                {
                    Console.WriteLine(e);
                    //Falls Loging verwendet wird, Funktion hier eintragen
                }
            }

            return max_Zoom;
        }

        public int getMinZoomLevel() //erstellt von Stefan Stöhr
        {
            int min_Zoom = 0;
            String s;
            IDbCommand dbcmd = connection.CreateCommand();
            dbcmd.CommandText = "select value from metadata where name like '%min_Zoom%'";

            try
            {
                IDataReader reader = dbcmd.ExecuteReader();

                if (reader.Read())
                {
                    s = reader.GetString(0);
                    min_Zoom = int.Parse(s);
                }
            }
            catch (SqliteException e)
            {
                Console.WriteLine(e);
                //Falls Loging verwendet wird, Funktion hier eintragen
            }
            return min_Zoom;
        }


        public Bounds getBounds() //erstellt von Stefan Stöhr
        {
            Bounds bounds = null;

            IDbCommand dbcmd = connection.CreateCommand();
            dbcmd.CommandText = "select value from metadata where name like '%" + NAME_TYPE_BOUNDS + "%'";

            if (connection.State == ConnectionState.Open)
            {
                try
                {
                    IDataReader reader = dbcmd.ExecuteReader();
                    if (reader.Read())
                    {
                        bounds = new Bounds(reader.GetString(0));
                    }
                }
                catch (SqliteException e)
                {
                    Console.WriteLine(e);
                    //Falls Loging verwendet wird, Funktion hier eintragen
                }
            }

            return bounds;
        }


        public static void initMetadata(String file) //erstellt von Stefan Stöhr
        {
            IDbConnection connection;
            String input_file = file;
            int max_zoom = 0;
            int min_zoom = 0;
            String max_zoom_text = NAME_TYPE_MAX_ZOOM;
            String min_zoom_text = NAME_TYPE_MIN_ZOOM;

            //Datenbankverbindung wird geöffnet
            connection = new SqliteConnection("Data Source= " + input_file);

            try
            {
                connection.Open();
            }
            catch (SqliteException e)
            {
                Console.WriteLine(e);
                //Falls Loging verwendet wird, Funktion hier eintragen
            }

            //min_Zoom, max_Zoom werden aus Tile ausgelesen, kann demnach mehrere Minuten dauern
            IDbCommand read_cmd = connection.CreateCommand();

            read_cmd.CommandText = "select min(zoom_level), max(zoom_level) from tiles";

            try
            {
                IDataReader reader = read_cmd.ExecuteReader();
                if (reader.Read())
                {
                    min_zoom = reader.GetInt32(0);
                    max_zoom = reader.GetInt32(1);
                }
            }
            catch (SqliteException e)
            {
                Console.WriteLine(e);
                //Falls Loging verwendet wird, Funktion hier eintragen

            }

            //min_Zoom, max_Zoom werden in Metadata eingetragen
            IDbCommand write_cmd1 = connection.CreateCommand();
            IDbCommand write_cmd2 = connection.CreateCommand();
            write_cmd1.CommandText = "insert into metadata values ('" + min_zoom_text + "'," + min_zoom + ")";
            write_cmd2.CommandText = "insert into metadata values ('" + max_zoom_text + "'," + max_zoom + ")";

            try
            {
                write_cmd1.ExecuteNonQuery();
                Console.WriteLine("Befehl: '" + write_cmd1.CommandText + "' in Datenbank geschrieben");

                write_cmd2.ExecuteNonQuery();
                Console.WriteLine("Befehl: '" + write_cmd2.CommandText + "' in Datenbank geschrieben");

            }
            catch (SqliteException e)
            {
                Console.WriteLine(e);
                //Falls Loging verwendet wird, Funktion hier eintragen
            }
            connection.Close();
        }

    }
}

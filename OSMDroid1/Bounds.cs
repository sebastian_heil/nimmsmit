﻿using Osmdroid.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OSMDroid1
{
    //erstellt von Stefan Stöhr
    class Bounds
    {
        private double bound_a_x;
        private double bound_a_y;
        private double bound_b_x;
        private double bound_b_y;

        public const String A_X = "a_x";
        public const String A_Y = "a_y";
        public const String B_X = "b_x";
        public const String B_Y = "b_y";

        public const String point_A = "point_A";
        public const String point_B = "point_B";


        public Bounds(double bound_a_x, double bound_a_y, double bound_b_x, double bound_b_y) //erstellt von Stefan Stöhr
        {
            this.bound_a_x = bound_a_x;
            this.bound_a_y = bound_a_y;
            this.bound_b_x = bound_a_x;
            this.bound_b_y = bound_b_y;
        }
        public Bounds(String bounds) //erstellt von Stefan Stöhr
        {
            String[] bounds_splited = bounds.Split(new Char[] { ',' });
            //Teilt den String in ein Array von Strings auf



            if (bounds_splited.Count() == 4)
            {
                this.bound_a_x = (Convert.ToDouble(bounds_splited[1].Replace(".", ",")));
                this.bound_a_y = (Convert.ToDouble(bounds_splited[0].Replace(".", ",")));
                this.bound_b_x = (Convert.ToDouble(bounds_splited[3].Replace(".", ",")));
                this.bound_b_y = (Convert.ToDouble(bounds_splited[2].Replace(".", ",")));

            }


        }
        public void setBound(String bound_id, double value) //erstellt von Stefan Stöhr
        {
            //setzt den Wert für eine x oder y Koordinate von a oder b
            switch (bound_id)
            {
                case A_X: this.bound_a_x = value; break;
                case A_Y: this.bound_a_y = value; break;
                case B_X: this.bound_b_x = value; break;
                case B_Y: this.bound_b_y = value; break;
            }
        }
        public double getBound(String bound_id) //liefert den Wert für eine x oder y Koordinate von a oder b zurück
        {
            switch (bound_id)
            {
                case A_X: return this.bound_a_x;
                case A_Y: return this.bound_a_y;
                case B_X: return this.bound_b_x;
                case B_Y: return this.bound_b_y;

                default: return 0;
            }
        }
        public GeoPoint getGeoPoint(String point_id) //liefert einen GeoPoint (A,B) zurück
        {
            switch (point_id)
            {
                case point_A: return new GeoPoint(bound_a_x, bound_a_y);
                case point_B: return new GeoPoint(bound_b_x, bound_b_y);

                default: return null;
            }
        }
        public GeoPoint getCenter() //erstellt von Stefan Stöhr
        {
            //liefert das Zentrum der Karte zurück
            if (isEmpty())
            {
                double diff_x = bound_b_x - bound_a_x;
                double diff_y = bound_b_y - bound_a_y;

                double center_x = bound_b_x - (diff_x / 2);
                double center_y = bound_b_y - (diff_y / 2);

                return new GeoPoint(center_x, center_y);
            }
            else
                return null;
        }
        public Boolean isEmpty() //erstellt von Stefan Stöhr
        {
            //überprüft ob die Bounds leer sind
            if (bound_a_x != 0 && bound_a_y != 0 && bound_b_x != 0 && bound_b_y != 0)
            {
                return true;
            }
            else
                return false;
        }
    }
}

﻿using Android.App;
using Android.Content;
using Android.Views;
using Android.OS;

/*
 * 
 * Erstellt von Daniel Polzhofer
 * */

namespace OSMDroid1
{
    [Activity(Label = "PSS Maps", MainLauncher = true, Icon = "@drawable/icon")]

    public class MainActivity : Activity
    {



        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            this.ActionBar.NavigationMode = ActionBarNavigationMode.Tabs;
            var tabMaps = this.ActionBar.NewTab();
            tabMaps.SetText("Maps");
            var tabDownload = this.ActionBar.NewTab();
            tabDownload.SetText("Downloaded");
            MapFragment mapFragment = new MapFragment();
            DownloadFragment downloadFragment = new DownloadFragment();

            tabMaps.TabSelected += delegate (object sender, ActionBar.TabEventArgs e)
            {
                e.FragmentTransaction.Replace(Resource.Id.fragments, mapFragment, "mapfragment");
            };

            tabDownload.TabSelected += delegate (object sender, ActionBar.TabEventArgs e)
            {
                e.FragmentTransaction.Replace(Resource.Id.fragments, downloadFragment, "downloadfragment");
            };


            this.ActionBar.AddTab(tabMaps);
            this.ActionBar.AddTab(tabDownload);

            SetContentView(Resource.Layout.Main);


        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater inflater = this.MenuInflater;
            inflater.Inflate(Resource.Menu.import_menu, menu);


            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Resource.Id.action_import)
            {
                Intent intent = new Intent(this, typeof(FileExplorer.FileExplorer));
                StartActivity(intent);
                this.Finish();

            }

            return true;

        }
        public override void OnBackPressed()
        {
            base.OnBackPressed();
        }

    }
}
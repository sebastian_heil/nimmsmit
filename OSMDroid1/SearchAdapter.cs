using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;


/*
 * 
 * Erstellt von Daniel Polzhofer
 * voraussichtlich nicht im Einsatz
 * */
namespace OSMDroid1
{
    class SearchAdapter : ArrayAdapter <Location>
    {
        private Context c;
        private int id;
        private List<Location> location;

   

         public SearchAdapter(Context context, int textViewResourceId, List<Location> locations)
            : base(context, textViewResourceId, locations)
        {
            c = context;
            id = textViewResourceId;
            location = locations;
        }

         public Location GetFileItem(int i)
         {
             return location[i];
         }



         public override View GetView(int position, View convertView, ViewGroup parent)
         {
             View v = convertView;

             if (v == null)
             {
                 LayoutInflater li = (LayoutInflater)c.GetSystemService(Context.LayoutInflaterService);
                 v = li.Inflate(id, null);
             }


             Location l = location[position];
             if (l != null)
             {
                 TextView t1 = (TextView)v.FindViewById(Resource.Id.list_text);
                 t1.SetText("qadfds", TextView.BufferType.Normal);

             }
             return v;
         }
    }
}
﻿using Osmdroid.Views.Overlay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OSMDroid1
{
    class MapOverlay
    {
        private string name;
        private TilesOverlay to;
        private OverlayItem olItem;
        private ItemizedIconOverlay gpsOverlay;


        public MapOverlay(string name, OverlayItem olItem)
        {
            // TODO: Complete member initialization
            this.name = name;
            this.olItem = olItem;
        }

        public MapOverlay(string name, ItemizedIconOverlay gpsOverlay)
        {
            // TODO: Complete member initialization
            this.name = name;
            this.gpsOverlay = gpsOverlay;
        }

        public MapOverlay(string name, TilesOverlay to)
        {
            // TODO: Complete member initialization
            this.name = name;
            this.to = to;
        }

        public string GetOverlayName()
        {
            return name;
        }

        public TilesOverlay GetTileOverlay()
        {
            return to;
        }

        public ItemizedIconOverlay GetItemizedIconOverlay()
        {
            return gpsOverlay;
        }

        public OverlayItem GetOverlayItem()
        {
            return olItem;
        }
    }
}

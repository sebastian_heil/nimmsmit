using System;
using System.Text;
using System.Net;
using System.IO;
using Java.Util;
using Java.IO;
using Android.Content;

namespace OSMDroid1
{
    class RequestService
    {
        private static readonly RequestService instance = new RequestService();

        private string username;

        private string pw;

        private string tenantId;

        private ArrayList geoCloudMaps = new ArrayList();

        private const string FILENAME = "GeoCloudMaps.txt";

        private string token;

        public static RequestService getInstance()
        {
            return instance;
        }

        //login
        public void loadToken(string tenantId, string name, string pw)
        {
            try
            {
                //for testing 
                name = "jusits@rmdata-geospatial.com";
                pw = "0bVu556HigsAuzommzJGDaHnNK0gJkV5pogrgcNg8Oaf5NZBlD%2FAiXL61Hlr8gyT5swIVgk4QY3%2FApsJVA3WQQ%3D%3D";
                tenantId = "htladmin";

                HttpWebRequest loginRequest = null;
                HttpWebResponse loginResponse = null;
                string loginUrl = "http://htladmin.rmdatacloud.com:8080/token";
                WebHeaderCollection loginHeader = new WebHeaderCollection();

                loginHeader.Set("TenantId", tenantId);


                loginRequest = (HttpWebRequest)WebRequest.Create(loginUrl);
                loginRequest.Method = WebRequestMethods.Http.Post;
                loginRequest.Headers = loginHeader;
                loginRequest.ContentType = "application/x-www-form-urlencoded";
                Stream loginStream = loginRequest.GetRequestStream();
                byte[] body = Encoding.UTF8.GetBytes("grant_type=password&username=" + name + "&password=" + pw);
                loginStream.Flush();
                loginStream.Write(body, 0, body.Length);

                loginResponse = (HttpWebResponse)loginRequest.GetResponse();

                if (loginResponse != null)
                {
                    this.username = name;
                    this.pw = pw;
                    this.tenantId = tenantId;
                    
                    Stream loginResponseStream = loginResponse.GetResponseStream();
                    int bufferSize = 1024;
                    byte[] readBytes = new byte[bufferSize];
                    loginResponseStream.Read(readBytes, 0, bufferSize);
                    string authorization = Encoding.UTF8.GetString(readBytes);
                    authorization = authorization.Remove(0, 17);
                    string[] bearer = authorization.Split("\"".ToCharArray());
                    authorization = "Bearer " + bearer[0];
                    this.token = authorization;
                    
                }
                
            }
            catch (Exception e)
            {
                throw new InvalidLoginException();
            }
        }

        public string getUserName()
        {
            return username;
        }

        public string getPw()
        {
            return pw;
        }

        public string getTenantId()
        {
            return tenantId;
        }

        public ArrayList getMapsInGeoCloud()
        {
            return geoCloudMaps;
        }

        public ArrayList loadMapsInGeoCloud(Context context)
        {
            ArrayList geoCloudMaps = new ArrayList(); 
            try
            {
                geoCloudMaps = readGeoCloudMaps(context);
            }
            catch (Exception e) 
            {

                HttpWebRequest request = null;
                HttpWebResponse response = null;
                WebHeaderCollection headers = new WebHeaderCollection();
                string url = "http://htladmin.rmdatacloud.com:8080/api/maps";

                //Load maps
                if (token != null)
                {
                    try
                    {

                        headers.Set("Authorization", token);

                        request = (HttpWebRequest)WebRequest.Create(url);
                        request.Headers = headers;
                        request.Method = WebRequestMethods.Http.Get;
                        request.Headers = headers;
                        response = (HttpWebResponse)request.GetResponse();
                    }
                    catch (Exception ex)
                    {
                        throw new NoGeoCloudAccessException();
                    }
                }
                if (response != null)
                {
                    Stream responseStream = response.GetResponseStream();
                    int bufferSize = 1024;
                    byte[] readBytes = new byte[bufferSize];
                    int count;
                    while ((count = responseStream.Read(readBytes, 0, bufferSize)) != 0)
                    {
                        string mapname = Encoding.UTF8.GetString(readBytes);
                        mapname.Replace("[", "");
                        mapname.Replace("\"", "");
                        mapname.Replace("]", "");
                        geoCloudMaps.Add(mapname);

                        Stream fos = context.OpenFileOutput(FILENAME, FileCreationMode.Private);
                        fos.Write(readBytes, 0, count);

                    }
                }
            }
            
            this.geoCloudMaps = geoCloudMaps;
            return geoCloudMaps;
        }

        private ArrayList readGeoCloudMaps(Context context)
        {
            ArrayList maps = new ArrayList();

            Stream fis = context.OpenFileInput(FILENAME);
            int bufferSize = 1024;
            byte[] readBytes = new byte[bufferSize];
            int count;
            while ((count = fis.Read(readBytes, 0, bufferSize)) != 0)
            {
                maps.Add(Encoding.UTF8.GetString(readBytes));
            }
            return maps;
        }
        private static string pwGeneration(string value)
        {
            using (var sha512 = System.Security.Cryptography.SHA512Managed.Create())
            {
                return Convert.ToBase64String(sha512.ComputeHash(Encoding.UTF8.GetBytes(value)));
            }

        }

        public void downloadMap(String mapName, String mapType, double minX, double minY, double maxX, double maxY)
        {

            string url = " http://htladmin.rmdatacloud.com:8080/api/maps/" + mapType + "/wms?SERVICE=WMS&REQUEST=GetMap&BBOX=" + minX + "," + minY + "," + maxX + "," + maxY + "&WIDTH=1090&Height=750&Layers=" + mapType + "&FORMAT=image/png&CRS=EPSG:3857&VERSION=1.3.0&Styles=&TRANSPARENT=TRUE";
            HttpWebRequest request = null;
            HttpWebResponse response = null;
            WebHeaderCollection headers = new WebHeaderCollection();

            try
            {
                request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = WebRequestMethods.Http.Get; ;
                headers.Set("Accept", "image/png");
                headers.Set("User-Agent", "WMSLayer");
                headers.Set("Host", "htladmin.rmdatacloud.com:8080");
                headers.Set("Connection", "Close");
                request.Headers = headers;
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (Exception ex)
            {
                throw new DownloadImageException();
            }

            if (response != null)
            {
                Stream responseStream = response.GetResponseStream();
                int bufferSize = 1024;
                byte[] readBytes = new byte[bufferSize];
                int count;

                using (var fileStream = System.IO.File.Create(mapName))
                {
                    while ((count = responseStream.Read(readBytes, 0, bufferSize)) != 0)
                    {
                        fileStream.Write(readBytes, 0, count);
                    }
                }

                String dir = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                Java.IO.File mbFolder = new Java.IO.File(dir, "mbtiles");
                Java.IO.File file;
                if (!mbFolder.Exists())
                {
                    mbFolder.Mkdir();
                    file = new Java.IO.File(mbFolder.AbsolutePath);
                }
                else
                {

                    file = new Java.IO.File(mbFolder.AbsolutePath);
                }
            }
        }

    }
}
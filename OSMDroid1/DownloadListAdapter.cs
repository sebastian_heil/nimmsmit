using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Java.Util;

namespace OSMDroid1
{
    class DownloadListAdapter : BaseAdapter
    {
        private Context context;

        private ArrayList list;
        public DownloadListAdapter(Context context)
        {
            this.context = context;
            list = new ArrayList();
        }

        public override int Count
        {
            get
            {
                return list.Size();
            }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return list.Get(position);
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            TextView mapText;
            Button downloadButton;

            convertView = LayoutInflater.From(context).Inflate(Resource.Layout.DownloadListItem, parent, false);
            mapText = (TextView)convertView.FindViewById(Resource.Id.downloadmaptext);
            downloadButton = (Button)convertView.FindViewById(Resource.Id.downloadmapbutton);
            mapText.Text = GetItem(position).ToString();
            downloadButton.SetOnClickListener(new OnDownloadButtonClickedListener());
            return convertView;
        }

        private class ViewHolder
        {
            public TextView mapText;
            public Button downloadButton;

            public ViewHolder(TextView t, Button b)
            {
                mapText = t;
                downloadButton = b;
            }
        }

        public void updateList(ArrayList list)
        {
            this.list = list;
            base.NotifyDataSetChanged();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Mono.Data.Sqlite;
using System.Data;

/*
 *  Changelog
 *  28.11.2014 - STÖHR: Listunterstützung und Try-Catch-Blöcke hinzugefügt
 */

namespace OSMDroid1
{
    /*
     * DAO enthält folgende Funktionen:
     * getLocationByName(String name);
     * getLocationByID(int id);
     * insertLocation(Location loc);
     * deleteLocationByID(int id);
     * deleteLocationByName(String name);
     * 
     */

    //erstellt von Stefan Stöhr, erweitert von Daniel Polzhofer
    class LocationDAO
    {
        private String input_file;
        private IDbConnection connection;

        public LocationDAO(String path) //erstellt von Stefan Stöhr, erweitert von Daniel Polzhofer
        {

            this.input_file = path;

            connection = new SqliteConnection("Data Source=" + input_file);
            try
            {
                connection.Open();
            }
            catch (SqliteException e)
            {
                //Ausgabe Fehlermeldung
                Console.WriteLine("Datenbank :-)" + e);
            }
        }

        public List<Location> getLocationByName(String name) //erstellt von Stefan Stöhr, erweitert von Daniel Polzhofer
        {
            List<Location> Loc_List = new List<Location>(); //liefert eine Liste der Ergebnisse zurück
            IDbCommand dbcmd = connection.CreateCommand();
            dbcmd.CommandText = "Select * from locations where name like '%" + name + "%'";

            if (connection.State == ConnectionState.Open)
            {
                try
                {
                    IDataReader reader = dbcmd.ExecuteReader();

                    while (reader.Read())
                    {
                        int _id = reader.GetInt32(0);
                        double cord_x = reader.GetDouble(1);
                        double cord_y = reader.GetDouble(2);
                        String _name = reader.GetString(3);
                        Loc_List.Add(new Location(_id, cord_x, cord_y, _name));
                    }
                }
                catch (SqliteException e)
                {
                    //Ausgabe Fehlermeldung
                    Console.WriteLine(e);

                }
            }
            if (Loc_List.Count == 0)
            {
                Loc_List = null;
            }
            System.Console.WriteLine("Suchanfrage durchgeführt für :' " + name + "'");

            return Loc_List;
        }

        public List<Location> getLocationByID(int id) //erstellt von Stefan Stöhr, erweitert von Daniel Polzhofer
        {
            List<Location> Loc_List = new List<Location>(); //liefert eine Liste der Ergebnisse zurück
            IDbCommand dbcmd = connection.CreateCommand();
            dbcmd.CommandText = "Select * from locations where id = " + id;

            if (connection.State == ConnectionState.Open)
            {
                try
                {
                    IDataReader reader = dbcmd.ExecuteReader();

                    while (reader.Read())
                    {
                        int _id = reader.GetInt32(0);
                        double cord_x = reader.GetDouble(1);
                        double cord_y = reader.GetDouble(2);
                        String _name = reader.GetString(3);
                        Loc_List.Add(new Location(_id, cord_x, cord_y, _name));
                    }
                }
                catch (SqliteException e)
                {
                    //Ausgabe Fehlermeldung
                    Console.WriteLine(e);
                }
            }
            if (Loc_List.Count == 0)
            {
                Loc_List = null;
            }
            System.Console.WriteLine("Suchanfrage durchgeführt für :' " + id + "'");

            return Loc_List;
        }

        public Boolean addLocation(int i, long x, long y, String name)
        {
            IDbCommand add_cmd = connection.CreateCommand();
            add_cmd.CommandText = "insert into Locations values (" + i + "," + x + "," + y + ",'" + name + "')";
            Boolean check = true;

            try
            {
                add_cmd.ExecuteNonQuery();
            }
            catch (SqliteException e)
            {
                Console.WriteLine(e);
            }

            return check;
        }
    }
}

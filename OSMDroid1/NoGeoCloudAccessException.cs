using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace OSMDroid1
{
    class NoGeoCloudAccessException : Exception
    {
        public NoGeoCloudAccessException() { }

        public NoGeoCloudAccessException(string message) : base(message) { }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

/*
   Erstellt von Daniel Polzhofer
 */

namespace OSMDroid1.FileExplorer
{
    public class FileItem
    {
        private String name;
        private bool isFile;
        private String path;
        private long size;
        private long date;
        public FileItem()
        {
            
        }

        public FileItem(String name, bool isFile, String path, long date)
        {
            this.name = name;
            this.isFile = isFile;
            this.path = path;
            this.date = date;
        }

        public FileItem(String name, bool isFile, String path, long size, long date)
        {
            this.name = name;
            this.isFile = isFile;
            this.path = path;
            this.size = size;
            this.date = date;
        }

        public String GetName()
        {
            return name;
        }

        public String GetPath()
        {
            return path;
        }

        public bool IsFile()
        {
            return isFile;
        }

        public override string ToString()
        {
            return this.GetName();
        }

        public String GetSize()
        {
            double help = (double)size;
            int i;
            string[] format = new string[]
            {
               "{0} bytes", "{0} KB", "{0} MB", "{0} GB"
            };

            for (i = 0; i < format.Length && help >= 1024; i++)
            {
                help = help / 1024;
            }

            return string.Format(format[i], (Math.Round(help,1).ToString("0.0")));  

        }

        public DateTime GetDate()
        {
            DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Local);
            DateTime dt = start.AddMilliseconds(date).ToLocalTime();
            return dt;

        }
    }
}
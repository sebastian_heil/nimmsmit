using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Java.Util;
using System;

namespace OSMDroid1
{

    class MapFragment : ListFragment
    {
        private Java.IO.File[] files;

        public MapFragment()
        {

        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            

        }

        public override View OnCreateView(LayoutInflater inflater,
        ViewGroup container, Bundle savedInstanceState)
        {

            var view = base.OnCreateView(inflater, container, savedInstanceState);
            ShowFiles();
            
            return view;
        }

        private void ShowFiles()
        {
            var arrayList = new ArrayList();
            String dir = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            Java.IO.File mbFolder = new Java.IO.File(dir, "mbtiles");

            if (!mbFolder.Exists())
            {
                mbFolder.Mkdir();
                files = new Java.IO.File(mbFolder.AbsolutePath).ListFiles();
            }
            else
            {

                files = new Java.IO.File(mbFolder.AbsolutePath).ListFiles();
            }



            if (files != null && files.Length > 0)
            {

                for (int i = 0; i < files.Length; i++)
                {
                    if (files[i].Name.EndsWith(".mbtiles") && files != null)
                    {

                        arrayList.Add(files[i].Name);
                    }
                }
            }

            else
            {

                arrayList.Add("Keine Karten oder Pl�ne vorhanden");
                files = null;

            }

            var adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, arrayList.ToArray());

            this.ListAdapter = adapter;
            
            /*
            listView.ItemLongClick += (sender, e) =>
            {
                OnListItemLongClick(sender, e);
            };
            */
            
        }


        private void OnListItemLongClick(object s, AdapterView.ItemLongClickEventArgs empf)
        {
            if (files != null)
            {
                Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(this.Activity);
                Android.App.AlertDialog alertDialog = builder.Create();
                alertDialog.SetTitle("Karte l�schen");
                alertDialog.SetMessage("Wollen Sie diese Karte wirklich l�schen?");
                alertDialog.SetButton("Nein", (sender, e) =>
                {

                });

                alertDialog.SetButton2("Ja", (sender, e) =>
                {
                    var file = files[empf.Position];
                    file.Delete();
                    ShowFiles();
                });
                alertDialog.Show();
            }

        }
        /*
        private void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            if (files != null)
            {

                var item = listView.GetItemAtPosition(e.Position).ToString();
                var mbtiles = new Intent(this.Activity, typeof(Map));

                mbtiles.PutExtra("Filename", item);
                StartActivity(mbtiles);
            }
        }
        */

        public override void OnListItemClick(ListView l, View v, Int32 position, Int64 id)
        {
            if (files != null)
            {

                var item = l.GetItemAtPosition(position).ToString();
                var mbtiles = new Intent(this.Activity, typeof(Map));

                mbtiles.PutExtra("Filename", item);
                StartActivity(mbtiles);
            }
        }

    }

}

